/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.didichuxing.tools.test;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.apache.commons.io.FileUtils;

public class MainAbility extends Ability implements IInterface.Callback<String> {
    HiLogLabel lable = new HiLogLabel(HiLog.LOG_APP,0x11,"test");
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        HiLog.error(lable,"start....");
        modify();
    }
    private void modify(){
        //测试三方库引入
        Log.d("xxx",FileUtils.getTempDirectoryPath());
        //Replace test

        //Log.d(TAG, "onCreate: ");//test method call replace

        ExampleSpec example = new ExampleSpec(0);
        String name = ExampleSpec.name; //field get
        ExampleSpec.name = "test";//field set

        // Insert test
        example.run(); //method call

        //test inner class

//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //method exec
//                Log.d(TAG, "onClick: ");
//            }
//        });
        ExampleSpec example2 = new ExampleSpec();
        int id = example2.id;//field get
        example2.id = 1;//field set

        // Around test
        example2.call();

        ExampleSpec example3 = new ExampleSpec("test");
        int id2 = example3.id2;//field get
        example3.id2 = 2;//field set

        //Enhance

        //Try catch test
        startAbility(null);
        //ohos.aafwk.ability.Ability.startAbility()throws IllegalArgumentException, IllegalStateException

        //Timing test
        ExampleSpec test4 = new ExampleSpec(1, 2);
        test4.timing();

        //Annotation test
        ExampleSpec test5 = new ExampleSpec(1, 2, 3);
        test5.test();
        test5.test2();
        test5.uncaught2();

        onCallback("");

        Child.main(null);
    }
    @Override
    public void onCallback(String value) {
        System.out.println("onCallback");
    }
}
