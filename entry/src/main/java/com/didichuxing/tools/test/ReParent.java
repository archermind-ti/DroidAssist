package com.didichuxing.tools.test;


public class ReParent extends Parent {

    protected int age;

    public ReParent(int age) {
        super(age);
    }

    protected void methodB() {
        System.out.println("ReParent ---methodB");
    }
}
