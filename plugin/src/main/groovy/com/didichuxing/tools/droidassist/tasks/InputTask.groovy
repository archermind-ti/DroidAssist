package com.didichuxing.tools.droidassist.tasks

import com.didichuxing.tools.droidassist.DroidAssistContext
import com.didichuxing.tools.droidassist.ex.DroidAssistBadStatementException
import com.didichuxing.tools.droidassist.ex.DroidAssistException
import com.didichuxing.tools.droidassist.ex.DroidAssistNotFoundException
import com.didichuxing.tools.droidassist.util.Logger
import javassist.CannotCompileException
import javassist.NotFoundException
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.TaskAction

import java.util.regex.Pattern


abstract class InputTask extends DefaultTask {

    public static final String DOT_CLASS = ".class"
    public static final String DOT_JAR = ".jar"
    DroidAssistContext droidAssistContext;
    String currentMode = "Debug"

    public InputTask(Project project,String mode){
        currentMode = mode
        droidAssistContext = new DroidAssistContext(project)
    }
    abstract void doTaskAction();

    @TaskAction
    void executeTask(){

        droidAssistContext.configure()
        getDeptFiles().each {
            //println it.path
            droidAssistContext.classPool.appendClassPath(it)
        }
        if(!droidAssistContext.extension.enable){
            return
        }
        doTaskAction();
        Logger.close()
    }

    @InputFiles
    abstract public ConfigurableFileCollection getDeptFiles();

    boolean executeClass(String className, File directory) {
        if(!packageMatch(className)){
            return false
        }
        //println("className---------"+className)

        def inputClass = null
        inputClass = droidAssistContext.classPool.getOrNull(className)
        if (inputClass == null) {
            return false
        }

        droidAssistContext.transformers.each {
            try {
                it.performTransform(inputClass, className)
            } catch (NotFoundException e) {
                throw new DroidAssistNotFoundException(
                        "Transform failed for class: ${className}" +
                                " with not found exception: ${e.cause?.message}", e)
            } catch (CannotCompileException e) {
                throw new DroidAssistBadStatementException(
                        "Transform failed for class: ${className} " +
                                "with compile error: ${e.cause?.message}", e)
            } catch (Throwable e) {
                throw new DroidAssistException(
                        "Transform failed for class: ${className} " +
                                "with error: ${e.cause?.message}", e)
            }
        }

        if (inputClass.modified) {
            inputClass.writeFile(directory.absolutePath)
            return true
        }
        return false
    }


    boolean packageMatch(String classname){
        if(droidAssistContext.extension.includePackages.size()==0){
            return true
        }
        boolean tag = false;
        droidAssistContext.extension.includePackages.each {
            if(Pattern.matches(it, classname)){
                tag= true
            }
        }
        return tag
    }
}
