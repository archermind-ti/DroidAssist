package com.didichuxing.tools.droidassist

import com.didichuxing.tools.droidassist.tasks.DirInputTask
import com.huawei.ohos.build.HarmonyAppPluginMain
import com.huawei.ohos.build.HarmonyHapPluginMain
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.tasks.TaskState

/**
 * Plugin entrance
 */
class DroidAssistPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.getPlugins().apply(HarmonyHapPluginMain.class);
        project.extensions.create("droidAssistOptions", DroidAssistExtension);
        //创建一个configuration
        Configuration dataFiles = project.getConfigurations().create("imp", { c ->
            c.setVisible(false);
            c.setCanBeConsumed(false);
            c.setCanBeResolved(true);
        });
        //创建插件
        if (project.plugins.hasPlugin(HarmonyHapPluginMain.class)) {
            ["Debug", "Release"].each {
                attachTask(project, it)
            }
        }
        //为插件添加依赖
        project.getTasks().withType(DirInputTask.class).configureEach(
                { dataProcessing ->
                    dataProcessing.getDeptFiles().from(dataFiles)
                });
    }

    void attachTask(Project project, String model) {
        project.getTasks().create("modify${model}Class", DirInputTask.class, project,model);
        def deployTasks = project.rootProject.getTasksByName("modify${model}Class", true)
        project.tasks.whenTaskAdded { task ->
            if (task.name == "compile${model}JavaWithJavac") {
                deployTasks.each { cc ->
                    cc.dependsOn(task)
                }
            }
            if (task.name == "generate${model}JsManifest") {
                deployTasks.each { cc ->
                    task.dependsOn(cc)
                }
            }

        }
    }

}
