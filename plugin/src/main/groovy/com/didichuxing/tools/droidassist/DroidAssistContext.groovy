package com.didichuxing.tools.droidassist

import com.didichuxing.tools.droidassist.ex.DroidAssistException
import com.didichuxing.tools.droidassist.transform.Transformer
import com.didichuxing.tools.droidassist.util.IOUtils
import com.didichuxing.tools.droidassist.util.Logger
import javassist.ClassPool
import javassist.DirClassPath
import org.gradle.api.Project

import java.util.stream.Collectors

/**
 * Context for plugin build env
 */
class DroidAssistContext {

    Project project
    DroidAssistClassPool classPool
    DroidAssistExtension extension
    Collection<Transformer> transformers

    DroidAssistContext(
            Project project) {
        this.project = project
    }

    def configure() {
        this.extension = project.droidAssistOptions
        Logger.init(
                extension.logLevel < 0 ? Logger.LEVEL_CONSOLE : extension.logLevel,
                extension.logDir ?: project.file("${project.buildDir}/outputs/logs/"))

        try {
            classPool = new DroidAssistClassPool()
//            project.configurations.compileClasspath.collect{
//                println("classpath:  "+it.path)
//                classPool.appendClassPath(it)
//            }

        } catch (Throwable e) {
            e.printStackTrace()
            throw new DroidAssistException("Failed to create class pool", e)
        }


    }

    def loadClassFile(){
        transformers = loadConfiguration()
    }

    def loadConfiguration() {
        if(extension == null){
            return null;
        }
        //println(classPool.toString())
        def transformers = extension.configFiles
                .parallelStream()
                .flatMap {
                    try {
                        def list = new DroidAssistConfiguration(project).parse(it)
                        return list.stream().peek {
                            transformer ->
                                transformer.classFilterSpec.addIncludes(extension.includes)
                                transformer.classFilterSpec.addExcludes(extension.excludes)
                                transformer.setClassPool(classPool)
                                transformer.setAbortOnUndefinedClass(extension.abortOnUndefinedClass)
                                transformer.check()
                        }
                    } catch (Throwable e) {
                        throw new DroidAssistException("Unable to load configuration," +
                                " unexpected exception occurs when parsing config file:$it, " +
                                "What went wrong:\n${e.message}", e)
                    }
                }//parse each file
                .collect(Collectors.toList())

        Logger.info("Dump transformers:")
        transformers.each {
            Logger.info("transformer: $it")
        }
        return transformers
    }



    def appendDirClassPath(String dir) {
        classPool.appendClassPath(new DirClassPath(dir))

    }

    def getSdkPath(){
        def localProperties = new Properties()
        localProperties.load(new FileInputStream(project.rootProject.file("local.properties")))
        return localProperties["hwsdk.dir"]
    }

    class DroidAssistClassPool extends ClassPool {
        DroidAssistClassPool() {
            super(true)
            childFirstLookup = true
        }

        void appendBootClasspath(Collection<File> paths) {
            paths.stream().parallel().forEach {
                appendClassPath(it)
                Logger.info "Append boot classpath: ${IOUtils.getPath(it)}"
            }
        }

        void appendClassPath(File path) {
            appendClassPath(path.absolutePath)
        }
    }
}