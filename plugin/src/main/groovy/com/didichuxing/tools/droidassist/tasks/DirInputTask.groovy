package com.didichuxing.tools.droidassist.tasks

import com.didichuxing.tools.droidassist.util.Logger
import com.didichuxing.tools.droidassist.util.WorkerExecutor
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction

import javax.inject.Inject
import java.nio.file.Files
import java.util.stream.Collectors

/**
 * 没必增量，这种方式会导致javac 任务重新编译
 */
abstract class DirInputTask extends InputTask{
    File inputDirFile;

    @Inject
    DirInputTask(Project project,String mode) {
        super(project,mode)
        inputDirFile =new File(project.buildDir.toString()+"/intermediates/javac/${mode}/classes")

    }



    @Override
    void doTaskAction() {
        droidAssistContext.appendDirClassPath(inputDirFile.path)
        droidAssistContext.loadClassFile()

        def executor = new WorkerExecutor(1)

        def fileList = Files.walk(inputDirFile.toPath())
                .parallel()
                .map { it.toFile() }//Path to file
                .filter { it.isFile() }
                .filter { it.name.endsWith(DOT_CLASS) } //Filter class file
                .collect(Collectors.toList())


        fileList.stream()
                .filter { it.isFile() }//Path to file
                .filter { it.name.endsWith(DOT_CLASS) }//Filter class file
                .forEach {
                    try{
                        executeClass(it, inputDirFile, temporaryDir)
                    }catch(Exception e){
                        e.printStackTrace()
                    }

                }

        executor.execute {
            FileUtils.copyDirectory(temporaryDir, inputDirFile)
        }

        executor.finish()

    }


    void executeClass(File classFile, File inputDir, File tempDir) {
        def className =
                FilenameUtils.
                        removeExtension(
                                inputDir.toPath()
                                        .relativize(classFile.toPath())
                                        .toString())
                        .replace(File.separator, '.')

        executeClass(className, tempDir)
    }





}
