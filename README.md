
# DroidAssist 
`DroidAssist` 是一个轻量级的字节码编辑插件，基于 `Javassist` 对字节码操作，根据 xml 配置处理 class 文件，以达到对 class 文件进行动态修改的效果

#### 特点

- 灵活的配置化方式，使得一个配置就可以处理项目中所有的 class 文件。
- 丰富的字节码处理功能，针对移动端的特点提供了例如代码替换，添加try catch，方法耗时等功能。
- 简单易用，只需要依赖一个插件，处理过程以及处理后的代码中也不需要添加额外的依赖。
- 处理速度较快，只占用较少的编译时间。

#### 功能
![avatar](docs/pic1.png)

#### 安装教程

DroidAssist 适用于 `DevEco` 工程 `library model` 或者 `hap model`，使用 DroidAssist 需要接入 DroidAssist 插件并编写专有配置文件。

###### 1.在`项目根目录` 的 `build.gradle`  里添加：

```groovy
buildscript {
    repositories {

        maven {
            url 'https://mirrors.huaweicloud.com/repository/maven/'
        }
        maven {
            url 'https://developer.huawei.com/repo/'
        }
        mavenCentral()
        jcenter()
    }
    dependencies {
        classpath 'com.gitee.archermind-ti:droidassist:1.0.0-beta'
    }
}
```

###### 2.在需要使用的 `模块` 的 build.gradle 里添加：

```groovy
apply plugin: 'com.didichuxing.tools.droidassist'
droidAssistOptions {
    config file("droidassist.xml"),file("droidassist2.xml") //插件配置文件(必选配置,支持多配置文件)
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation "commons-io:commons-io:2.4"
    implementation 'junit:junit:4.12'
    //项目中使用到的依赖需要告诉插件
    imp "commons-io:commons-io:2.4"
    //ohos.jar 所在目录
    //如: imp fileTree(dir: 'E:\\Huawei\\Sdk\\java\\2.1.1.21\\api', include: ['*.jar', '*.har']) 
    imp fileTree(dir: '{your sdk dir}', include: ['*.jar', '*.har'])
}
```

###### 3.其他配置：
* `enable` 如果需要停用 DroidAssist 插件功能，可以添加 `enable false` 以停用插件 (可选配置)
* `logLevel` 日志输出等级：`0` 关闭日志输出，`1` 输出日志到控制台 `2` 输出日志到文件 `3` 输出日志到控制台以及日志 (可选配置)
* `logDir` 日志输出目录，当日志输出到文件时，默认的输出目录是当前 `model` 的 `build/outputs/logs` 目录 (可选配置)
* `includePackages` 需要处理的类包名，默认为全部 (可选配置)
* `imp` 字节码修改所需的依赖库引用方法，`{your sdk dir}`为SDK目录


#### 使用文档
- 完整开发文档和配置见 [开发文档wiki](docs/wiki.md)

#### Demo运行注意事项

- 运行前，请按安装教程里说明，修改ohos.jar目录为本地SDK目录。

#### 源码安装步骤
1. 需要先将plugin模块上传到maven(本地或远程仓库都可以)
2. 在root build.gradle -> buildscript ->repositories 中添加地址，dependencies 中添加依赖包
3. 在需要使用插件的模块下 build.gradle 添加`apply plugin:'com.didichuxing.tools.droidassist'`
4. 配置droidassist 需要使用的参数

#### 局限性

- 由于 Javassist 的机制，DroidAssist 在处理的过程中将会产生额外的局部变量用以指向参数变量和保存返回值，但处理后有一些局部变量并没有实际作用。
- DroidAssist 在处理某些代码时可能会新增一些额外的代理方法。


####  版权和许可信息
  - Apache License Version 2.0

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.